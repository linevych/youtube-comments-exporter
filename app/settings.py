import os


class Settings:
    API_KEY = os.getenv('API_KEY', None)
    API_MAX_RESULTS = os.getenv('API_MAX_RESULTS', 100)

    PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
    BASE_DIR = os.path.dirname(PROJECT_ROOT)

    if not API_KEY:
        raise ValueError('API_KEY is not set')

    TEMPLATE_FOLDER = os.getenv('TEMPLATE_FOLDER', os.path.join(PROJECT_ROOT, 'templates'))
    STATIC_FOLDER = os.getenv('STATIC_FOLDER', os.path.join(BASE_DIR, 'static'))

    BABEL_DEFAULT_LOCALE = os.getenv('BABEL_DEFAULT_LOCALE', 'en')
    BABEL_DEFAULT_TIMEZONE = os.getenv('BABEL_DEFAULT_TIMEZONE', 'UTC')
    SECRET_KEY = os.getenv('SECRET_KEY', 'foobar')
    JSON_AS_ASCII = os.getenv('JSON_AS_ASCII', False)
    JSON_SORT_KEYS = False

    SUPPORTED_LANGUAGES = {
        'uk': 'Ukrainian',
        'en': 'English'
    }
