import datetime
import logging
from collections import OrderedDict
from typing import List, Dict

from google import Api

logger = logging.getLogger(__file__)


def process_comment(comment: dict) -> dict:
    try:
        comment = comment['snippet']['topLevelComment']['snippet']
    except KeyError:
        comment = comment.get('snippet')

        if comment is None:
            logger.error(f'Comment has no key snippet: {comment}')
            return {}

    date_published = datetime.datetime.strptime(comment.get('publishedAt'), '%Y-%m-%dT%H:%M:%S.%fZ')

    return OrderedDict({
        'user_name': comment.get('authorDisplayName'),
        'channel_url': comment.get('authorChannelUrl'),
        'text': comment.get('textOriginal'),
        'date_published': f'{date_published.day}/{date_published.month}/{date_published.year}',
        'like_count': comment.get('likeCount', 0)
    })


def unique_by(target: List[Dict], key: str) -> List[Dict]:
    return list({v[key]: v for v in target}.values())


def filter_comments(raw_comments: List[Dict], exclude_video_author=True, author_channel_url=None) -> List[Dict]:
    comments = unique_by([process_comment(comment) for comment in raw_comments], 'channel_url')

    if exclude_video_author:
        comments = [comment for comment in comments if author_channel_url not in comment['channel_url']]
    return comments


def export_comments(video_url: str, exclude_video_author: bool = False) -> List[Dict]:
    channel_url = None
    api = Api()
    comments = api.get_comments(video_url=video_url)

    if exclude_video_author:
        channel_url = api.get_video_channel_url(video_url)

    return filter_comments(comments, exclude_video_author=exclude_video_author, author_channel_url=channel_url)
