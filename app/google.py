import logging
from http import HTTPStatus
from typing import List, Dict
from urllib.parse import parse_qs, urlparse

import flask
import requests

import constants
from app import app

logger = logging.getLogger(__file__)
API_KEY = app.config.get('API_KEY')
API_MAX_RESULTS = app.config.get('API_MAX_RESULTS')


def get_video_id(video_url):
    video_id = parse_qs(urlparse(video_url).query).get('v')

    if video_id is None:
        flask.abort(constants.VIDEO_NOT_FOUND)

    return video_id[0]


class ApiMethod:
    api_key = API_KEY
    status_validation = {}
    url = NotImplemented

    def __call__(self, *args, **kwargs):
        raise NotImplementedError

    def process_request(self, url, params, **kwargs):
        response = requests.get(url, params=params)
        self.validate_response(response)
        return response.json()

    def validate_response(self, response, **kwargs):
        if response.status_code == HTTPStatus.OK:
            return

        flask.abort(self.status_validation.get(response.status_code, constants.UNEXPECTED_ERROR))
        logger.error(f'{response.status_code}: {response.json()}')


class GetComments(ApiMethod):
    url = 'https://www.googleapis.com/youtube/v3/commentThreads'
    status_validation = {
        HTTPStatus.NOT_FOUND: constants.VIDEO_NOT_FOUND,
        HTTPStatus.FORBIDDEN: constants.VIDEO_IS_PRIVATE,
    }

    def __call__(self, video_url: str, replies: bool = True, *args, **kwargs) -> List[Dict]:
        part = ['snippet']

        if replies:
            part.append('replies')

        video_id = get_video_id(video_url)

        params = {
            'videoId': video_id,
            'part': ','.join(part),
            'key': self.api_key,
            'maxResults': API_MAX_RESULTS,
        }
        return self.process_request(self.url, params)

    def process_request(self, url, params, **kwargs):
        page_token = True
        results = []
        while page_token:
            response = requests.get(url, params=params)
            self.validate_response(response, **kwargs)
            data = response.json()
            page_token = data.get('nextPageToken', None)
            result = data.get('items', [])
            results += result
            for r in result:
                for comment in r.get('replies', {}).get('comments', []):
                    results.append(comment)

            params['pageToken'] = page_token

        return results


class GetVideoChannelUrl(ApiMethod):
    url = 'https://www.googleapis.com/youtube/v3/videos'
    status_validation = {
        HTTPStatus.NOT_FOUND: constants.VIDEO_NOT_FOUND
    }

    def __call__(self, video_url, *args, **kwargs) -> str:
        video_id = get_video_id(video_url)

        params = {
            'id': video_id,
            'key': API_KEY,
            'part': 'snippet',
            'fields': 'items(snippet(channelId))'
        }
        return self.process_request(self.url, params, **kwargs)

    def process_request(self, url, params, **kwargs):
        response = super().process_request(url, params, **kwargs)

        try:
            return response['items'][0]['snippet']['channelId']
        except KeyError as e:
            flask.abort(constants.UNEXPECTED_ERROR)
            logger.error(f'{e}')


class Api:
    get_comments = GetComments()
    get_video_channel_url = GetVideoChannelUrl()
