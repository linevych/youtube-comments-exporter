from http import HTTPStatus

import flask
from flask_babel import lazy_gettext as _

LANGUAGE_URL_KWARG = 'lang_code'

RESPONSE_FIELDS = {
    '#': '#',
    'user_name': _('Name'),
    'text': _('Text'),
    'date_published': _('Published'),
    'like_count': _('Likes count')
}

UNEXPECTED_ERROR = flask.Response(_('An unexpected error occurred, please try again.'),
                                  HTTPStatus.INTERNAL_SERVER_ERROR.value)
VIDEO_NOT_FOUND = flask.Response(_('Video does not exists'), status=HTTPStatus.NOT_FOUND.value)
VIDEO_IS_PRIVATE = flask.Response(_('This video is not public'), status=HTTPStatus.FORBIDDEN.value)
CHANNEL_IS_PRIVATE = flask.Response(_('This channel is not public'), status=HTTPStatus.FORBIDDEN.value)
