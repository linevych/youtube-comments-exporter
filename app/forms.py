from flask_babel import lazy_gettext as _

from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField
from wtforms.validators import DataRequired, Regexp


class ExtractCommentsForm(FlaskForm):
    video_url = StringField(
        'video_url',
        validators=[DataRequired(),
                    Regexp(r'(https?:\/\/)?(www\.)?youtube.com\/watch\?v\=[A-z0-9]\w+.*', message=_('Invalid URL'))]
    )
    exclude_author = BooleanField('exclude_author', default=True)
