import flask
from flask import views

from base import export_comments as export_comments_util
from forms import ExtractCommentsForm
from constants import RESPONSE_FIELDS

main = flask.Blueprint('main', __name__)


class HomeView(views.MethodView):
    def get(self):
        return flask.render_template('index.html')

    def post(self):
        form = ExtractCommentsForm()
        data = flask.request.get_json()
        if form.validate_on_submit():
            url = data.get('video_url')
            exclude_author = data.get('exclude_author', True)
            response = {
                'results': export_comments_util(video_url=url, exclude_video_author=exclude_author),
                'fields': RESPONSE_FIELDS,
            }
            return flask.jsonify(response)
        else:
            return flask.make_response(flask.jsonify(form.errors), 400)


main.add_url_rule('/', view_func=HomeView.as_view('home'))
