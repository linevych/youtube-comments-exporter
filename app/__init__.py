import flask
from flask_babel import Babel
from flask_wtf import CSRFProtect

from settings import Settings
from constants import LANGUAGE_URL_KWARG

app = flask.Flask(__name__, template_folder=Settings.TEMPLATE_FOLDER, static_folder=Settings.STATIC_FOLDER)
app.config.from_object(Settings)
babel = Babel(app)
CSRFProtect(app)


def register_blueprints():
    from views import main
    app.register_blueprint(main, url_prefix='/')
    app.register_blueprint(main, url_prefix=f'/<{LANGUAGE_URL_KWARG}>/')


register_blueprints()


@babel.localeselector
def get_locale():
    return flask.g.get(LANGUAGE_URL_KWARG, app.config['BABEL_DEFAULT_LOCALE'])


@app.url_defaults
def set_language_code(endpoint, values):
    if LANGUAGE_URL_KWARG in values or flask.g.get(LANGUAGE_URL_KWARG, None) is None:
        return

    if app.url_map.is_endpoint_expecting(endpoint, LANGUAGE_URL_KWARG):
        values[LANGUAGE_URL_KWARG] = flask.g.lang_code


@app.url_value_preprocessor
def get_lang_code(endpoint, values):
    if values is not None:
        flask.g.lang_code = values.pop(LANGUAGE_URL_KWARG, None)


@app.before_request
def ensure_lang_support():
    lang_code = flask.g.get(LANGUAGE_URL_KWARG, None)
    if lang_code and lang_code not in app.config['SUPPORTED_LANGUAGES'].keys():
        return flask.abort(404)
