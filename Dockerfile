FROM node:8.11.4-stretch as frontend_builder
RUN mkdir -p /app
WORKDIR /app
COPY ./ ./
ARG environment=prod
ENV PATH="/app/node_modules/.bin:${PATH}"
RUN yarn install && gulp build_$environment

FROM tiangolo/uwsgi-nginx-flask:python3.6
ARG environment=prod
COPY --from=frontend_builder /app/static ./static
COPY ./ ./
ENV PYTHONPATH="/app/app:${PYTHONPATH}"
RUN pip install -r requirements/$environment.txt
RUN pybabel compile -d app/translations
RUN cp conf/uwsgi.ini ./