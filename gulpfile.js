'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const gulpConcat = require('gulp-concat');
const gulpRename = require('gulp-rename');
const jshint = require('gulp-jshint');
const babel = require("gulp-babel");
const babelConfig = {presets: ['es2015']};

const devPath = 'static/';
const prodPath = devPath;
const scssSrc = './assets/sass/*.scss';
const cssDir = 'css/';
const jsDir = 'js/';
const jsGlob = jsDir + '*.js';

const jsCompileMap = {
  "js/main.js": ["assets/js/main.js"],
};

// Sass

gulp.task('scss_dev', function () {
  return gulp.src(scssSrc)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(devPath + cssDir));
});

gulp.task('scss_prod', function () {
  return gulp.src(scssSrc)
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(cleanCSS({compatibility: 'ie8', level: 2}))
    .pipe(gulp.dest(prodPath + cssDir));
});


// JS

gulp.task('js_dev', function () {
  for (let outputFile in jsCompileMap) {
    gulp.src(jsCompileMap[outputFile])
      .pipe(sourcemaps.init())
      .pipe(babel(babelConfig))
      .pipe(gulpConcat(outputFile))
      .pipe(gulpRename(outputFile))
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest(devPath))
  }
});


gulp.task('js_prod', function () {
  for (let outputFile in jsCompileMap) {
    gulp.src(jsCompileMap[outputFile])
      .pipe(babel(babelConfig))
      .pipe(gulpConcat(outputFile))
      .pipe(gulpRename(outputFile))
      .pipe(uglify())
      .pipe(gulp.dest(prodPath))
  }
});


gulp.task('lint', function () {
  return gulp.src(jsGlob)
    .pipe(jshint()).pipe(jshint.reporter('jshint-stylish'))
});


gulp.task('build_dev', ['scss_dev', 'lint', 'js_dev']);
gulp.task('build_prod', ['scss_prod', 'lint', 'js_prod']);