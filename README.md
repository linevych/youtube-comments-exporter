# Youtube Comments Exporter

## Installation

```bash
 docker build . -t youtube_comments_exporter:latest --no-cache
 docker run -e API_KEY=<GOOGLE_API_KEY> -p 80:80 youtube_comments_exporter 
```

## Environmental variables
- `API_KEY` (**Default**: None) — Google API Key. To get it you need to create a project in 
  [Developers Console](https://console.developers.google.com/apis/dashboard) then click on "Enable APIs and Services" 
  and add "YouTube Data API v3". Go to Credentials tab, click Create credentials and choose API key.
- `BABEL_DEFAULT_LOCALE` (**Default**: en) — Application default language. You can check available translations in 
   `app/translations`.