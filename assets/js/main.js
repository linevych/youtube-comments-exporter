const form = document.querySelector('#main-form');
const submitButton = document.querySelector('#main-form .submitButton');
const tableHolder = document.querySelector('.results');

form.onsubmit = e => {
  // stop the regular form submission
  e.preventDefault();

  // collect the form data while iterating over the inputs
  let data = {};
  let i = 0, ii = form.length;
  for (; i < ii; ++i) {
    let input = form[i];
    if (input.name) {
      data[input.name] = input.value;
    }
  }

  // construct an HTTP request
  let xhr = new XMLHttpRequest();
  xhr.open(form.method, form.action, true);
  xhr.setRequestHeader("X-CSRFToken", document.getElementsByName('csrf_token')[0].value);
  xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

  clearErrorMessages();
  toggleProgressAnimation();
  // send the collected data as JSON
  xhr.send(JSON.stringify(data));
  removeAllChids(document.querySelector('.results'));
  xhr.onloadend = () => {
    toggleProgressAnimation();

    if (xhr.status === 200) {
      tableHolder.appendChild(makeResponsesTableFromJSON(JSON.parse(xhr.response)));
    }
    try {
      const response = JSON.parse(xhr.response);
      for (let key in response) {
        if (response.hasOwnProperty(key)) {
          let errors = response[key];
          for (let i = 0; i < errors.length; i++) {
            appendErrorToField('video_url', errors[i])
          }
        }
      }
    }
    catch (e) {
      appendErrorToField('video_url', xhr.response)
    }
  }
};


function appendErrorToField(name, error) {
  const errorMessage = document.createElement('span');
  errorMessage.className = 'error';
  errorMessage.textContent = error;
  insertAfter(errorMessage, form.querySelector(`[name="${name}"]`))
}

function insertAfter(newNode, referenceNode) {
  referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function clearErrorMessages() {
  let errors = document.querySelectorAll('.error'), i;
  for (i = 0; i < errors.length; ++i) {
    errors[i].parentNode.removeChild(errors[i])
  }
}

function toggleProgressAnimation() {
  submitButton.classList.toggle('submitButton--progress')
}

function removeAllChids(node) {
  while (node.hasChildNodes()) {
    node.removeChild(node.lastChild);
  }
}

function makeResponsesTableFromJSON(data, options) {
  const defaultOptions = {
    className: 'table',
    headings: data['fields'],
  };
  let table = document.createElement('table');
  let title = document.createElement('tr');
  let results = data['results'];

  options = Object.assign(defaultOptions, options, options);

  if (!Array.isArray(results)) {
    throw `"data" must be array not ${typeof data}`;
  }

  if (results.length === 0) {
    throw '"data" should not be empty';
  }

  table.className = options.className;

  for (let heading in options.headings) {
    let th = document.createElement('th');
    th.appendChild(document.createTextNode(options.headings[heading]));
    title.appendChild(th);
  }
  table.appendChild(title);

  let counter = 1;

  for (let row of results) {
    let tr = document.createElement('tr');
    for (let heading in options.headings) {
      let td = document.createElement('td');

      if (heading === 'user_name') {
        let link = document.createElement('a');
        link.href = row['channel_url'];
        link.appendChild(document.createTextNode(row[heading]));
        td.appendChild(link);
      }
      else if (heading === '#') {
        td.appendChild(document.createTextNode(counter));
        counter++;
      }
      else {
        td.appendChild(document.createTextNode(row[heading]));
      }
      tr.appendChild(td)
    }
    table.appendChild(tr)
  }
  return table
}

